package it.step.gotham.jail.service.interfaces;

import java.util.List;

import org.json.JSONObject;

import it.step.gotham.jail.entity.Guard;

public interface IGuard {
	boolean update(Guard guard);
	boolean delete(Guard guard);
	Guard findById(int id);
	List<Guard> findByStatus(String status);
	List<Guard> findAll();
	List<Guard> findAll(int page, int size, String sort, String filter, boolean order);
	long count();
	boolean validate(JSONObject json);
}
