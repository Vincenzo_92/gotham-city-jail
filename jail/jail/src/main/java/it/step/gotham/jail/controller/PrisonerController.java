package it.step.gotham.jail.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.step.gotham.jail.entity.Dossier;
import it.step.gotham.jail.entity.Prisoner;
import it.step.gotham.jail.service.interfaces.IPrisoner;
import it.step.gotham.jail.util.Util;

@RestController
@CrossOrigin
@RequestMapping({"prisoners"})
public class PrisonerController {

	@Autowired
	IPrisoner service;

	Util util = new Util("UTC");
	
	@ResponseBody
	@PostMapping(value = "insert", produces = MediaType.APPLICATION_JSON_VALUE)
	public String addGuard(@RequestHeader HttpHeaders head, @RequestBody String request) {
		JSONObject json = new JSONObject(request);
		String text = "";
		boolean flag = false;
		if(json.has("firstName") && json.has("lastName") && 
				json.has("birthDate") && json.has("status") && 
				json.has("dossier")) {
			if(service.validate(json)) {
				Prisoner prisoner = util.setPrisoner(json);
				if(prisoner != null && service.update(prisoner)) {
					text = "Successful insertion";
					flag = true;
				} else {
					text = "ERROR: Insertion not occurred";
				}
			} else {
				text = "ERROR: Uncorrect syntax";
			}
		} else {
			text = "ERROR: Missing fields!";
		}
		return new JSONObject().put("flag", flag).put("text", text).toString();
	}
	
	@ResponseBody
	@PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateGuard(@RequestHeader HttpHeaders head, @RequestBody String request) {
		JSONObject json = new JSONObject(request);
		String text = "";
		boolean flag = false;
		if(json.has("id") && json.has("firstName") && json.has("lastName") && 
				json.has("birthDate") && json.has("status") &&
				json.has("dossier")) {
			if(service.validate(json)) {
				Prisoner prisoner = util.setPrisoner(json);
				if(prisoner != null && service.update(prisoner)) {
					text = "Successful modification";
					flag = true;
				} else {
					text = "ERROR: modification not occurred";
				}
			} else {
				text = "ERROR: Uncorrect syntax";
			}
		} else {
			text = "ERROR: Missing fields!";
		}
		return new JSONObject().put("flag", flag).put("text", text).toString();
	}
	
	@ResponseBody
	@DeleteMapping(value = "delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteGuard(@RequestHeader HttpHeaders head, @RequestBody String request) {
		JSONObject json = new JSONObject(request);
		String text = "";
		boolean flag = false;
		if(json.has("id")) {
			Prisoner prisoner = service.findById(json.getInt("id"));
			if(prisoner != null && service.delete(prisoner)) {
				text = "Successful cancellation";
				flag = true;
			} else {
				text = "ERROR: cancellation not occurred";
			}
		} else {
			text = "ERROR: Missing fields!";
		}
		return new JSONObject().put("flag", flag).put("text", text).toString();
	}
	
	@ResponseBody
	@GetMapping(value = "getById", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getById(@RequestHeader HttpHeaders head, @RequestParam int id) {
		Prisoner prisoner = service.findById(id);
		if(prisoner != null)
			return new JSONObject().put("prisoner",prisoner).toString();
		else
			return null;
	}
	
	@ResponseBody
	@GetMapping(value = "getByStatus", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,Object> getByStatus(@RequestHeader HttpHeaders head, @RequestParam String status) {
		List<Prisoner> list = service.findByStatus(status);
		if(!list.isEmpty()) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("list", list);
			return map;
		} else {
			return null;
		}
	}
	
	@ResponseBody
	@GetMapping(value = "getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,Object> getById(@RequestHeader HttpHeaders head) {
		List<Prisoner> list = service.findAll();
		if(!list.isEmpty()) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("list", list);
			return map;
		} else {
			return null;
		}
	}
	
	@ResponseBody
	@GetMapping(value = "getDossier", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getDossier(@RequestHeader HttpHeaders head, @RequestParam String first_name, @RequestParam String last_name) {
		Dossier dossier = service.findDossier(first_name, last_name);
		if(dossier!=null) {
			return new JSONObject().put("dossier", dossier).toString();
		} else {
			return null;
		}
	}
	
	@ResponseBody
	@GetMapping(value = "getAllPaged", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,Object> getByPage(@RequestHeader HttpHeaders head, @RequestParam int page, @RequestParam int size, 
			@RequestParam String sort, @RequestParam String filter, @RequestParam boolean order) {
		List<Prisoner> list = service.findAll(page, size, sort, filter, order);
		if(!list.isEmpty()) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("list", list);
			return map;
		} else {
			return null;
		}
	}
}
