package it.step.gotham.jail.service.impl;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.step.gotham.jail.entity.Dossier;
import it.step.gotham.jail.entity.Prisoner;
import it.step.gotham.jail.jpa.repository.PrisonerRepository;
import it.step.gotham.jail.service.interfaces.IPrisoner;
import it.step.gotham.jail.util.Validation;

@Service
public class PrisonerService implements IPrisoner {
	
	@Autowired
	PrisonerRepository repository;

	@Override
	public boolean update(Prisoner prisoner) {
		try {
			repository.save(prisoner);
			return true;
		} catch(IllegalArgumentException e) {
			return false;
		}
		
	}

	@Override
	public boolean delete(Prisoner prisoner) {
		repository.delete(prisoner);
		return findById(prisoner.getId()) == null;
	}

	@Override
	public Prisoner findById(int id) {
		return repository.findById(id);
	}

	@Override
	public List<Prisoner> findByStatus(String status) {
		return repository.findByStatus(status);
	}

	@Override
	public List<Prisoner> findAll() {
		return repository.findAll();
	}

	@Override
	public List<Prisoner> findAll(int page, int size, String sort, String filter, boolean order) {
		Pageable pageable = null;
		if(sort.equals("")) {
			pageable = PageRequest.of(page-1, size);
		} else {
			if(order)
				pageable = PageRequest.of(page-1, size, Sort.by(sort).ascending());
			else
				pageable = PageRequest.of(page-1, size, Sort.by(sort).descending());		
		}
		if(filter.equals(""))
			return repository.findAll(pageable).getContent();
		else
			return repository.findByFilter(filter, pageable).getContent();
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public Dossier findDossier(String firstName, String lastName) {
		return repository.findDossier(firstName, lastName);
	}
	
	@Override
	public boolean validate(JSONObject json) {
		Validation validate = new Validation();
		boolean flag = false;
		boolean death = true;
		boolean transfer = true;
		boolean layoff = true;
		boolean release = true;
		flag = validate.validField(json.getString("firstName"),validate.getRegex().get("firstName")) && 
				validate.validField(json.getString("lastName"),validate.getRegex().get("lastName")) && 
				validate.validField(json.getString("status"),validate.getRegex().get("statusPrisoner")) && 
				validate.validField(json.getString("birthDate"),validate.getRegex().get("birthDate")) &&
				validate.validField(json.getString("crimList"),validate.getRegex().get("crimList")) &&
				(validate.validField(json.getString("deathDate"),validate.getRegex().get("allDate")) || !json.has("deathDate")) &&
				(validate.validField(json.getString("transferDate"),validate.getRegex().get("allDate")) || !json.has("transferDate")) &&		
				validate.validField(json.getString("imprisonmentDate"),validate.getRegex().get("allDate")) &&
				(validate.validField(json.getString("releaseDate"),validate.getRegex().get("allDate")) || !json.has("releaseDate")) &&
				(validate.validField(json.getString("escapeDate"),validate.getRegex().get("allDate")) || !json.has("escapeDate"));
		
		if(json.has("deathDate"))
			death = validate.validToday(json.getString("deathDate"));
		if(json.has("transferDate"))
			transfer = validate.validToday(json.getString("transferDate"));
		if(json.has("escapeDate"))
			layoff = validate.validToday(json.getString("escapeDate"));
		if(json.has("releaseDate"))
			release = validate.validToday(json.getString("releaseDate"));
		return flag && death && transfer && layoff && release;
	}

}
