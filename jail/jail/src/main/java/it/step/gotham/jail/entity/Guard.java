package it.step.gotham.jail.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "guards")
public class Guard extends Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(timezone = "GMT+01:00")
	private Date workStartDate;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(timezone = "GMT+01:00")
	private Date layoffDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(Date workStartDate) {
		this.workStartDate = workStartDate;
	}

	public Date getLayoffDate() {
		return layoffDate;
	}

	public void setLayoffDate(Date layoffDate) {
		this.layoffDate = layoffDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((layoffDate == null) ? 0 : layoffDate.hashCode());
		result = prime * result + ((workStartDate == null) ? 0 : workStartDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Guard other = (Guard) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (layoffDate == null) {
			if (other.layoffDate != null)
				return false;
		} else if (!layoffDate.equals(other.layoffDate))
			return false;
		if (workStartDate == null) {
			if (other.workStartDate != null)
				return false;
		} else if (!workStartDate.equals(other.workStartDate))
			return false;
		return true;
	}

	public Guard() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Guard(int id, String firstName, String lastName, Date birthDate, Date deathDate, Date transferDate, String status,
			Date workStartDate, Date layoffDate) {
		super(firstName, lastName, birthDate, deathDate, transferDate, status);
		this.id = id;
		this.workStartDate = workStartDate;
		this.layoffDate = layoffDate;
	}

	public Guard(String firstName, String lastName, Date birthDate, Date deathDate, Date transferDate, String status,
			Date workStartDate, Date layoffDate) {
		super(firstName, lastName, birthDate, deathDate, transferDate, status);
		this.workStartDate = workStartDate;
		this.layoffDate = layoffDate;
	}

		
}
