package it.step.gotham.jail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GothamCityJailApplication {

	public static void main(String[] args) {
		SpringApplication.run(GothamCityJailApplication.class, args);
	}

}
