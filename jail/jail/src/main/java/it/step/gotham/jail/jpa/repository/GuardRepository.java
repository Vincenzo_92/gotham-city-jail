package it.step.gotham.jail.jpa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.step.gotham.jail.entity.Guard;

@Repository
public interface GuardRepository extends JpaRepository<Guard,Integer>{
	
	Guard findById(int id);
	List<Guard> findByStatus(String status);
	List<Guard> findAll();
	Page<Guard> findAll(Pageable pageable);
	long count();
	
	@Query(value = "SELECT g FROM Guard g WHERE g.firstName LIKE %?1% OR g.lastName LIKE %?1% OR g.status LIKE %?1%")
	Page<Guard> findByFilter(String filter, Pageable pageable);

}
