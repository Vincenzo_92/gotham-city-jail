package it.step.gotham.jail.util;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
	private Map<String,String> regex = new HashMap<String,String>();
	
	public Map<String,String> getRegex(){
		return regex;
	}
	
	private void setRegex() {
		regex.put("firstName","^([a-z]{3,}|[a-z]{3,}\\s[a-z]{3,})$");
		regex.put("lastName","^([a-z]{3,}|[a-z]{2,}\\s[a-z]{3,})$");
		regex.put("birthDate","^((19|20)\\d\\d-(0[1-9]|1[012])-([012]\\d|3[01])T([01]\\d|2[0-3]):([0-5]\\d):([0-5]\\d).(\\d\\d\\d)Z)$");
		regex.put("allDate","^(19|20)\\d\\d-(0[1-9]|1[012])-([012]\\d|3[01])");
		regex.put("crimList","^([a-z\\s]+)$");
		regex.put("statusGuard","^(death|fired|transferred|work|rest)$");
		regex.put("statusPrisoner","^(death|transferred|escapee|jailed)$");
		
	}
	
	public boolean validField(String value, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value.toLowerCase());
		return matcher.matches();
	}
	
	public boolean validToday(String value) {
		String[] dateArray = value.split("T")[0].split("-");
		LocalDate data = LocalDate.of(Integer.parseInt(dateArray[0]), Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[2]));
		return LocalDate.now().isAfter(data);
	}
	
	public Validation() {
		setRegex();
	}
	

}
