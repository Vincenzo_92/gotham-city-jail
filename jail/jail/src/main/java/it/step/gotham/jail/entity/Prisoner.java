package it.step.gotham.jail.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="prisoners")
public class Prisoner extends Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(timezone = "GMT+01:00")
	private Date escapeDate;
	
	@OneToOne(fetch = FetchType.EAGER,  cascade = CascadeType.ALL)
	private Dossier dossier;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEscapeDate() {
		return escapeDate;
	}

	public void setEscapeDate(Date escapeDate) {
		this.escapeDate = escapeDate;
	}

	public Dossier getDossier() {
		return dossier;
	}

	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dossier == null) ? 0 : dossier.hashCode());
		result = prime * result + ((escapeDate == null) ? 0 : escapeDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prisoner other = (Prisoner) obj;
		if (dossier == null) {
			if (other.dossier != null)
				return false;
		} else if (!dossier.equals(other.dossier))
			return false;
		if (escapeDate == null) {
			if (other.escapeDate != null)
				return false;
		} else if (!escapeDate.equals(other.escapeDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Prisoner(Integer id, String firstName, String lastName, Date birthDate, Date deathDate, Date transferDate, 
			String status, Date escapeDate) {
		super(firstName, lastName, birthDate, deathDate, transferDate, status);
		this.id = id;
		this.escapeDate = escapeDate;
	}
	
	public Prisoner(String firstName, String lastName, Date birthDate, Date deathDate, Date transferDate,
			String status, Date escapeDate) {
		super(firstName, lastName, birthDate, deathDate, transferDate, status);
		this.escapeDate = escapeDate;
	}

	public Prisoner() {
		super();
	}
	
}
