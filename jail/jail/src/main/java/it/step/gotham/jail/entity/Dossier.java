package it.step.gotham.jail.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "dossiers")
public class Dossier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date imprisonmentDate;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date releaseDate;
	
	@Column
	private String crimeList;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "dossier")
	private Prisoner prisoner;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crimeList == null) ? 0 : crimeList.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imprisonmentDate == null) ? 0 : imprisonmentDate.hashCode());
		result = prime * result + ((prisoner == null) ? 0 : prisoner.hashCode());
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dossier other = (Dossier) obj;
		if (crimeList == null) {
			if (other.crimeList != null)
				return false;
		} else if (!crimeList.equals(other.crimeList))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imprisonmentDate == null) {
			if (other.imprisonmentDate != null)
				return false;
		} else if (!imprisonmentDate.equals(other.imprisonmentDate))
			return false;
		if (prisoner == null) {
			if (other.prisoner != null)
				return false;
		} else if (!prisoner.equals(other.prisoner))
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getImprisonmentDate() {
		return imprisonmentDate;
	}

	public void setImprisonmentDate(Date imprisonmentDate) {
		this.imprisonmentDate = imprisonmentDate;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getCrimeList() {
		return crimeList;
	}

	public void setCrimeList(String crimeList) {
		this.crimeList = crimeList;
	}

	public Prisoner getPrisoner() {
		return prisoner;
	}

	public void setPrisoner(Prisoner prisoner) {
		this.prisoner = prisoner;
	}

	public Dossier(Integer id, Date imprisonmentDate, Date releaseDate, String crimeList) {
		super();
		this.id = id;
		this.imprisonmentDate = imprisonmentDate;
		this.releaseDate = releaseDate;
		this.crimeList = crimeList;
	}
	
	public Dossier(Date imprisonmentDate, Date releaseDate, String crimeList) {
		super();
		this.imprisonmentDate = imprisonmentDate;
		this.releaseDate = releaseDate;
		this.crimeList = crimeList;
	}

	public Dossier() {
		super();
	}

}
