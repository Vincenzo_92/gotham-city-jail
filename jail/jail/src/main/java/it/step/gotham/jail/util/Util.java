package it.step.gotham.jail.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import it.step.gotham.jail.entity.Dossier;
import it.step.gotham.jail.entity.Guard;
import it.step.gotham.jail.entity.Prisoner;

public class Util {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	public Guard setGuard(JSONObject json){
		try {
			if (json.has("id"))
				return new Guard(
					json.getInt("id"), 
					json.getString("firstName"), 
					json.getString("lastName"),
					new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("birthDate")), 
					sdf.parse(json.getString("deathDate")),
					sdf.parse(json.getString("transferDate")),
					json.getString("status"),
					sdf.parse(json.getString("workStartDate")),
					sdf.parse(json.getString("layoffDate")));
			else
				return new Guard(
					json.getString("firstName"), 
					json.getString("lastName"),
					new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("birthDate")), 
					sdf.parse(json.getString("deathDate")),
					sdf.parse(json.getString("transferDate")),
					json.getString("status"),
					sdf.parse(json.getString("workStartDate")),
					sdf.parse(json.getString("layoffDate")));
		} catch (JSONException e) {
			return null;
		} catch (ParseException e) {
			return null;
		}
	}

	public Prisoner setPrisoner(JSONObject json) {
		try {
			Prisoner prisoner = null;
			Dossier dossier = setDossier(json.getJSONObject("dossier"));
			if (json.has("id")) {
				prisoner = new Prisoner(
					json.getInt("id"), 
					json.getString("firstName"), 
					json.getString("lastName"),
					new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("birthDate")), 
					sdf.parse(json.getString("deathDate")),
					sdf.parse(json.getString("transferDate")),
					json.getString("status"),
					sdf.parse(json.getString("escapeDate")));
			} else {
				prisoner = new Prisoner(
					json.getString("firstName"), 
					json.getString("lastName"),
					new SimpleDateFormat("yyyy-MM-dd").parse(json.getString("birthDate")), 
					sdf.parse(json.getString("deathDate")),
					sdf.parse(json.getString("transferDate")),
					json.getString("status"),
					sdf.parse(json.getString("escapeDate")));
			}
			prisoner.setDossier(dossier);
			dossier.setPrisoner(prisoner);
			return prisoner;
		} catch (JSONException e) {
			return null;
		} catch (ParseException e) {
			return null;
		}
	}

	public Dossier setDossier(JSONObject json) {
		try {
			if(json.has("id")) {
				return new Dossier(
					json.getInt("id"), 
					sdf.parse(json.getString("imprisonmentDate")),
					sdf.parse(json.getString("releaseDate")), 
					json.getString("crimeList"));
			} else {
				return new Dossier(
					sdf.parse(json.getString("imprisonmentDate")),
					sdf.parse(json.getString("releaseDate")), 
					json.getString("crimeList"));
			}
		} catch (JSONException e) {
			return null;
		} catch (ParseException e) {
			return null;
		}
	}

	public Util(String timeZone) {
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
	}
}
