package it.step.gotham.jail.service.interfaces;

import java.util.List;

import org.json.JSONObject;

import it.step.gotham.jail.entity.Dossier;
import it.step.gotham.jail.entity.Prisoner;

public interface IPrisoner {
	boolean update(Prisoner prisoner);
	boolean delete(Prisoner prisoner);
	Prisoner findById(int id);
	List<Prisoner> findByStatus(String status);
	List<Prisoner> findAll();
	List<Prisoner> findAll(int page, int size, String sort, String filter, boolean order);
	long count();
	Dossier findDossier(String firstName, String lastName);
	boolean validate(JSONObject json);
}
