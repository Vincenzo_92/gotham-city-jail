package it.step.gotham.jail.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.step.gotham.jail.entity.Guard;
import it.step.gotham.jail.jpa.repository.GuardRepository;
import it.step.gotham.jail.service.interfaces.IGuard;
import it.step.gotham.jail.util.Validation;

@Service
public class GuardService implements IGuard {

	@Autowired
	GuardRepository repository;

	@Override
	public boolean update(Guard guard) {
		try {
			repository.save(guard);
			return true;
		} catch(IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public boolean delete(Guard guard) {
		try {
			repository.delete(guard);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
	@Override
	public Guard findById(int id) {
		return repository.findById(id);
	}

	@Override
	public List<Guard> findByStatus(String status) {
		return repository.findByStatus(status);
	}

	@Override
	public List<Guard> findAll() {
		return repository.findAll();
	}

	@Override
	public List<Guard> findAll(int page, int size, String sort, String filter, boolean order) {
		Pageable pageable = null;
		if(sort.equals("")) {
			pageable = PageRequest.of(page-1, size);
		} else {
			if(order)
				pageable = PageRequest.of(page-1, size, Sort.by(sort).ascending());
			else
				pageable = PageRequest.of(page-1, size, Sort.by(sort).descending());		
		}
		if(filter.equals(""))
			return repository.findAll(pageable).getContent();
		else
			return repository.findByFilter(filter, pageable).getContent();
	}

	@Override
	public long count() {
		return repository.count();
	}
	
	@Override
	public boolean validate(JSONObject json) {
		Validation validate = new Validation();
		boolean flag = false;
		boolean death = true;
		boolean transfer = true;
		boolean layoff = true;
		flag = validate.validField(json.getString("firstName"),validate.getRegex().get("firstName")) && 
				validate.validField(json.getString("lastName"),validate.getRegex().get("lastName")) && 
				validate.validField(json.getString("status"),validate.getRegex().get("statusGuard")) && 
				validate.validField(json.getString("birthDate"),validate.getRegex().get("birthDate")) &&
				validate.validField(json.getString("workStartDate"),validate.getRegex().get("allDate")) &&
				(validate.validField(json.getString("deathDate"),validate.getRegex().get("allDate")) || !json.has("deathDate")) &&
				(validate.validField(json.getString("transferDate"),validate.getRegex().get("allDate")) || !json.has("transferDate")) &&		
				(validate.validField(json.getString("layoffDate"),validate.getRegex().get("allDate")) || !json.has("layoffDate"));
		
		if(json.has("deathDate"))
			death = validate.validToday(json.getString("deathDate"));
		if(json.has("transferDate"))
			transfer = validate.validToday(json.getString("transferDate"));
		if(json.has("layoffDate"))
			layoff = validate.validToday(json.getString("layoffDate"));
		
		return flag && death && transfer && layoff;
	}
}
