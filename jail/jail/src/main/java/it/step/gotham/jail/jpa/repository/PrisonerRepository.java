package it.step.gotham.jail.jpa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.step.gotham.jail.entity.Dossier;
import it.step.gotham.jail.entity.Prisoner;

@Repository
public interface PrisonerRepository extends JpaRepository<Prisoner, Integer> {
	
	Prisoner findById(int id);
	List<Prisoner> findByStatus(String status);
	List<Prisoner> findAll();
	Page<Prisoner> findAll(Pageable pageable);
	long count();
	
	@Query(value = "SELECT p FROM Prisoner p WHERE p.firstName LIKE %?1% OR p.lastName LIKE %?1% OR p.status LIKE %?1%")
	Page<Prisoner> findByFilter(String filter, Pageable pageable);
	
	@Query(value = "SELECT p.dossier FROM Prisoner p WHERE p.firstName = ?1 AND p.lastName = ?2")
	Dossier findDossier(String firstName, String lastName);
}
